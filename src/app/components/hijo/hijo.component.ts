import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() primerM!: string;
  @Input() segundM!: string;
  @Input() primerObj!: any;
  @Input() secObj!: any;

  //Output
  mensaje: string = 'Mensaje del hijo al padre';
  @Output() EventoMensaje = new EventEmitter<string>()

  mensaje2: string = 'segundo Mensaje';
  @Output() Evento2 = new EventEmitter<string>()

  jsonHijo: any = {
    fruta: 'sandia',
    verdura: 'zanahoria',
    carne: 'chancho'
  }
  @Output() Eventojson1 = new EventEmitter<string>()

  jsonHijo2: any = {
    desayuno: 'panqueques',
    almuerzo: 'chairo',
    cena: 'yogurt'
  }
  @Output() Eventojson2 = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje)
    this.Evento2.emit(this.mensaje2)
    this.Eventojson1.emit(this.jsonHijo)
    this.Eventojson2.emit(this.jsonHijo2)
  }

}
