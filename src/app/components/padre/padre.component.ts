import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  parent1: string = 'Primer variable';
  parent2: string = 'Segunda variable'
  obj1: any;
  obj2: any;

  //output
  mensaje1!: string;
  mensaje2!: string;
  json1!: any;
  json2!: any;

  constructor(public dataSvc: DataService) { }

  ngOnInit(): void {
    this.obj1 = this.dataSvc.Iobject
    this.obj2 = this.dataSvc.Iobj2
  }

  recibirMensaje($event: string): void{
    this.mensaje1 = $event;
    console.log(this.mensaje1);   
  }

  recibirM2($event: string): void{
    this.mensaje2 = $event;
    console.log(this.mensaje2);
  }

  recibirJson($event: any): void{
    this.json1 = $event
  }

  recibirJson2($event: any): void{
    this.json2 = $event
  }

}
